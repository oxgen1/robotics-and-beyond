//Import The Libraries We need. Http will make a web server for us and fs will read the file we need it to
const http = require('http');
const fs = require('fs');

// Create a Webserver
http.createServer(function (req, res) {
    //get the contents of our HTML file
    fs.readFile('client/index.html', function(err, data){
        //Tell the Browser, Hey were sending you some html please read it that way
        res.writeHead(200, {'Content-Type': 'text/html'});
        //Send the Contents of our HTML File
        res.write(data);
        //Theres nothing else we need to send to the browser so stop sending stuff.
        res.end();
    });

//Start the webserver on Port 8080: 
}).listen(8080);

