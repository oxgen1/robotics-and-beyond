//Import The Libraries We need. Http will make a web server for us and fs will read the file we need it to
const http = require('http');
const fs = require('fs');
const express = require('express');
const app = express();
const rpio = require('rpio');

var LEDPin = 12;

// Create a Webserver
http.createServer(function (req, res) {
    //get the contents of our HTML file
    fs.readFile('client/index.html', function(err, data){
        //Tell the Browser, Hey were sending you some html please read it that way
        res.writeHead(200, {'Content-Type': 'text/html'});
        //Send the Contents of our HTML File
        res.write(data);
        //Theres nothing else we need to send to the browser so stop sending stuff.
        res.end();
    });

//Start the webserver on Port 8080: 
}).listen(8888);

//Now you can goto localhost:8080/ and see the Contents of index.html

//Now we want to the website to talk to raspberry pi, In order todo we need to set up a "Route"

app.get('/toggleLED', function (req, res) {
    result = toggleLED();
    console.log(result);
    res.send(result);
});

app.listen(433);



rpio.open(LEDPin, rpio.OUTPUT, rpio.LOW);

function toggleLED(){ 
    console.log(rpio.read(LEDPin) == 1);
    if(rpio.read(LEDPin)){
        rpio.write(LEDPin, rpio.LOW);
        return 'LED Turned OFF'
    } else {
        rpio.write(LEDPin, rpio.HIGH);
        return 'LED Turned ON'
    } 
} 
